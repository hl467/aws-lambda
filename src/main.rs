use lambda_http::{run, service_fn, Body, Error, Request, RequestExt, Response};

//A marco polo function that return polo if you pass in Marco
fn marco_polo_greeting(greeting: &str) -> String {
    if greeting.contains("Hello") || greeting.contains("Hi") || greeting.contains("hello") || greeting.contains("hi") {
        "Hiiiii !! ".to_string()
    } else {
        ".............Silence.......".to_string()
        // greeting.to_string()
    }
}

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract some useful info from the request
    let query_params = event.query_string_parameters();
    let greeting = query_params.first("greeting").unwrap_or_else(|| "No greeting");

    // // Prepare the response
    // let resp = Response {
    //     req_id: event.context.request_id,
    //     msg: marco_polo_greeting(&greeting),
    // };

    let message = marco_polo_greeting(&greeting);

    let resp = Response::builder()
    .status(200)
    .header("content-type", "text/html")
    .body(message.into())
    .map_err(Box::new)?;

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
