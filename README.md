# Marco Polo Lambda Function

## Project Overview
This is an example of an AWS Lambda function built using Rust and `cargo lambda`. The function responds to HTTP requests with specific greetings ("Hello" or "Hi") and returns an appropriate response.

## Features
- Receives a `greeting` query parameter in an HTTP request.
- Responds with "Hiiiii !!" if the query parameter contains "Hello" or "Hi" (not case insensitive).
- Otherwise, responds with ".............Silence......." because the lambda is pretty shy.

## Technology Stack
- Rust
- `cargo lambda`
- AWS Lambda
- AWS API Gateway

## Environment Setup
Ensure your system has the following tools installed:
- Rust and Cargo ([Installation Guide](https://www.rust-lang.org/tools/install))
- `cargo lambda` (installed via the command `cargo install cargo-lambda`)

## Deployment and Execution
Follow these steps to deploy and run the Lambda function:

### Build the Lambda Function
Run the following command in the project root directory to build the Lambda function:
```bash
cargo lambda build --release --arm64
```

### Deploy to AWS Lambda
Ensure your AWS credentials are configured. Then deploy the function to AWS Lambda using:
```bash
cargo lambda deploy
```

### Interacting with the Lambda Function via API
The Lambda function can be accessed via the following API URL:
```
https://vwx72kwah8.execute-api.us-east-1.amazonaws.com/default/reserve-hello-lambda
```

To test the function, make an HTTP GET request to the above URL with a `greeting` query parameter. For example:
```
https://vwx72kwah8.execute-api.us-east-1.amazonaws.com/default/reserve-hello-lambda?greeting=Hello
```

A sample output is shown below:
<img src="lambda.png" alt="lambda.png">
